import os
from packaging.version import Version
from importlib.metadata import version

import blissdemo
from bliss.shell import standard
from bliss.config import static

_BLISS_VERSION = Version(version("bliss"))


def run_demo(bliss_session):
    bliss_session.setup_globals.user_script_homedir_oda()
    bliss_session.user_script_load("all")
    bliss_session.env_dict["user"].all_print()
    bliss_session.env_dict["user"].all_demo()


if __name__ == "__main__":
    os.environ["BEACON_HOST"] = "localhost:10001"
    os.environ["TANGO_HOST"] = "localhost:10000"
    os.environ["DEMO_ROOT"] = blissdemo.__path__[0]

    config = static.get_config()
    bliss_session = config.get("demo_session")

    if _BLISS_VERSION >= Version("2.1.0dev0"):
        bliss_session.active_session()

    env_dict = dict()
    env_dict.update(standard.__dict__)

    assert bliss_session.setup(env_dict=env_dict), "Session setup failed"
    run_demo(bliss_session)
    print("Exiting bliss demo process ...")
