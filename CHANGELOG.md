# CHANGELOG.md

## Unreleased

## 1.0.0

New features:

- Support blissdata 2.0
- `XrpdProcessor`: opt-in for data portal uploading
- `XrpdProcessor`: opt-in for forcing direct Lima file reading (instead of VDS)
- `Id31XrpdProcessor`: Opt-in for ASCII export
- `Id31XrpdProcessor`: load workflows from `ewoksid31` on the worker side
- `BM23XrpdProcessor`: XRPD data processing at BM23
- `ID24XrpdProcessor`: XRPD data processing at ID24
- `DaiquiriProcessorMixin`: Allow processer objects to be used in Daiquiri
- `DaiquiriXrpdProcessor`: `Id13XrpdProcessor` with Daiquiri support
- `Id13XrpdProcessor`: neural networks for background removal and phase prediction
- `ExafsProcessor`: support concatenated EXAFS scans for online EXAFS analysis
- `ExafsProcessor`: support concatenated EXAFS scans for offline EXAFS analysis (saved split HDF5 scans)
- `Id12Hdf5ToAsciiConverter`: allow filtering the counters to save
- `Id15bEiger2Crysalis`: convert Eiger to Esperanto data format

Changes:

- Drop support for bliss branch id31_2.0
- Deprecate any mentioning of "worker". It refers to the "Celery message queue".
- `Id31XrpdProcessor`: flat-field in now opt-in
- `ExafsPlotter` is renamed to `ExafsProcessor`
- `Bm23ExafsPlotter` is renamed to `Bm23ExafsProcessor`
- `Id24ExafsPlotter` is renamed to `Id24ExafsProcessor`

Bug fixes:

- `Id31StreamlineScanner`: protect optimization scans from pilatus protection errors

## 0.8.0

New features:

- `Id06XrpdProcessor`: pyFAI integration at ID06
- `Id11XrpdProcessor`: add NeXus saving for PDF data
- `Id22XrpdProcessor`: add support for multiple intergration configs
- `Id32SpecGenProcessor`: data processing for ID32
- `StreamlineScanner`: Add parameter to skip when no QR code can be read
- `TxsProcessor`: data processing for `txs` for ID09
- `XrpdPlotter`: add plots for 2D integrated patterns
- `XrpdProcessor`: new method `get_data` to access processed data (similar to `scan.get_data`).

Bug fixes:

- Fix bug in scan type determination in BM23 EXAFS plotter
- `PersistentNdArray`: Fix additional dimension added when retrieving 1D arrays
- `XrpdProcessor`: Fix plotter that could not be disabled

## 0.7.0

New features:

- `Id11Eiger2Crysalis`: convert HDF5 to Esperanto
- `Id10XrpdProcessor`: pyFAI integration at ID10
- `Id13XrpdProcessor`: support `CreateDiffMapFile` task
- `XrpdProcessor`: option to disable the plotter (for Daiquiri)
- `Id02SaxsProcessor`: SAXS data processing
- `Id02XpcsProcessor`: XPCS data processing

Changes:

- `Id31StreamlineScanner`: rockit of the sample is now optional
- `Id22XrpdProcessor`: support integration with different configurations

Bug fixes:

- XRPD plotter: fix URL
- `XrpdProcessor`: the `retry_period` was not passed to the workflow
- `XrpdProcessor`: pass `retry_timeout` and `retry_period` to saving tasks

## 0.6.0

New features:

- ID11: Eiger to Crysalis data format
- Streamline: rockit is optional

Changes:

- XRPD: log plotter task failures

Bug fixes:

- Fix HDF5 URL (remove scheme)

## 0.5.0

New features:

- ID24: temprature plotter
- ID13: XRPD processor

Changes:

- Use `importlib` to manage package resources
- XRPD processor: make sure the unparametrized workflow is always copied to the current proposal

## 0.4.2

Bug fixes:

- Demo: fix bug in ID14 workflow parameter gathering

## 0.4.1

Bug fixes:

- EXAFS: include unfinished scans when looking for the last scan number
- Demo: missing ewoksid12 dependency
- Demo: fix streamline workflow since we now have HDF5 and ASCII saving

## 0.4.0

Changes:

- BM23: support triggering of new EXAFS scans
- EXAFS: use pint registry from Bliss
- Streamline: replace "calibration" with "autocalibration" for clarity
- ID22: diffract22bliss -> lid22bliss
- Streamline: optimization per sample and per baguette
- Streamline: pyfai ring detection detector can be different from the calibration detector
- Streamline: support workflows that save ASCII files

## 0.3.1

Changes:

- The ExafsPlotter for the demo session uses the energy units from the motor if it specifies units

## 0.3.0

New features:

- Support Bliss 2.0
- Add ID22 XRPD workflows
- Add ID24 EXAFS workflows
- Add ID11 PDF workflows
- Add ID14 data conversion workflows

Changes:

- `WithPersistentParameters` module and defaults handling

## 0.2.0

New features:

- ID22 XRPD workflow

Changes:

- Better parameter inheritance support for `WithPersistentParameters`

## 0.1.0

Added:

- EXAFS plotting (BM23)
- SAXS/WAXS integration (ID31, BM02, ID11, ID09)
- STREAMLINE HTXRPD (ID31)
- ID22 XRD processor
