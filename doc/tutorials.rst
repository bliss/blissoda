Tutorials
=========

Exercises to learn the different aspects of automated data analysis in Bliss with Ewoks.
See the list of tutorials on the left.

.. toctree::
    :hidden:

    tutorials/installation
    tutorials/gettingstarted
    tutorials/demo
    tutorials/id22
    tutorials/id31
    tutorials/streamline
