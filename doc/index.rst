blissoda
========

*blissoda* provides utilities for online data analysis in `BLISS <https://gitlab.esrf.fr/bliss/bliss/>`_.

The *blissoda* library does not provide code for data processing nor has any scientific libraries as
dependencies. It contains the *BLISS*-side logic of online data processing:

* Manage workflow triggering
    * Decide when to trigger a workflow (end of every scan, start of every scan with a diffraction camera, etc.)
    * Provide workflow parameters
    * Enable/disable workflow triggering
* Handling workflow results
    * Plot workflow results in *Flint*

This logic can be specific to projects or beamlines.

*blissoda* is mostly used in the BLISS session setup scripts or user macros. In this case it needs to be
installed in the BLISS environment. Since the actual data processing happens remotely, *blissoda*
needs an `ewoksjob <https://gitlab.esrf.fr/workflow/ewoks/ewoksjob>`_ installation with workers
to perform the actual computations.

*blissoda* has been developed by the `Software group <http://www.esrf.eu/Instrumentation/software>`_ of the `European Synchrotron <https://www.esrf.eu/>`_.

.. toctree::
    :hidden:

    tutorials
    howtoguides
    api
