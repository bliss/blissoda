Installation
============

Since data processing is not done in Bliss itself but in external workers,
you need to install supporting libraries in the Bliss environment as well
as in one or more external python environments.

Client Side
+++++++++++

In the Bliss environment install with the `client` option

.. code:: bash

    pip install blissoda[client]

Beamline specific clients are installed with the beamline name as option

.. code:: bash

    pip install blissoda[id11]

Project specific clients are installed with the project name as option

.. code:: bash

    pip install blissoda[streamline]

External triggering
-------------------

When workflows are not triggered from Bliss but from an external process
that is monitoring Redis scan data, install with the `server` option in
the python environment in which this monitoring process needs to run

.. code:: bash

    pip install blissoda[server]

To start the monitoring process

.. code:: bash

    BEACON_HOST=id00:25000 blissoda workflows

Worker Side
+++++++++++

*blissoda* needs an `ewoksjob <https://ewoksjob.readthedocs.io>`_ installation
with workers for the actual computations.

A typical installation at an ESRF beamline is described :ref:`here <esrf_installation>`.
