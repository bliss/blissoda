Online Data Processing in the Bliss Demo session
================================================

.. info::

    For this part, you need to have a *BLISS* demo session installed. See the Bliss documentation for `how to install a demo session <https://bliss.gitlab-pages.esrf.fr/bliss/master/installation.html#demo-environment>`_.


Servers
-------

Start the Bliss demo servers

.. code:: bash

    bliss-demo-servers --lima-environment=lima_env [--oda-environment=oda_env]

Demo session
------------

Start the Bliss demo shell

.. code:: bash

    bliss-demo-session

Load user scripts from the *blissoda* project

.. code:: python

    DEMO_SESSION [1]: user_script_homedir_oda()

Run all examples
----------------

Run all data processing examples for testing purposes

.. code:: python

    DEMO_SESSION [2]: user_script_load("all")
    DEMO_SESSION [3]: user.all_demo()

Run specific examples
---------------------

Streamline
++++++++++

Automated calibration and integration of single-shot SAXS/WAXS (saving and ICAT upload)

.. code:: python

    DEMO_SESSION [2]: user_script_load("streamline")
    DEMO_SESSION [3]: user.streamline_demo(with_autocalibration=True)

XRPD
++++

Automated integration of SAXS/WAXS scans (saving and plotting in Flint)

.. code:: python

    DEMO_SESSION [2]: user_script_load("xprd")
    DEMO_SESSION [3]: user.xrpd_demo()

BM23
++++

Automated EXAFS processing during scans (plotting in Flint)

.. code:: python

    DEMO_SESSION [2]: user_script_load("bm23")
    DEMO_SESSION [3]: user.bm23_demo()

ID22
++++

Automated data processing specific to ID22 (saving)

.. code:: python

    DEMO_SESSION [2]: user_script_load("id22")
    DEMO_SESSION [3]: user.id22_stscan_demo()
    DEMO_SESSION [4]: user.id22_xrpd_demo()
