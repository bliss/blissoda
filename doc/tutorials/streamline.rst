Online Data Processing for Streamline HT-XRPD
=============================================

Fully automated Powder diffraction (scanning and data processing)

Usage
-----

.. code-block:: python

    DEMO_SESSION [1]: from blissoda.demo.streamline_scanner import streamline_scanner,sample_changer
    DEMO_SESSION [2]: streamline_scanner.eject()
    DEMO_SESSION [3]: streamline_scanner.load()
    DEMO_SESSION [4]: streamline_scanner.calib(0.1, sample_index=3)
    DEMO_SESSION [5]: streamline_scanner.run(0.1)

The arguments of `calib` and `run` are the same as `sct` with some additional keyword
arguments. The calibration is done by selecting a specific sample `sample_index`.

Installation
------------

Installation on lid31dat1 (data processing)
+++++++++++++++++++++++++++++++++++++++++++

One conda environment called `ewoksworker`

Dependencies installed:

.. code-block:: bash

    pip install ewoksjob[blissworker]
    pip install ewoksxrpd

Processes registered with the supervisor

- ewoksmonitor: monitor data processing http://lid31dat1:5555/
- ewoksworker: celery worker that does the actual calculation

Installation on bibhelm (acquisition control)
+++++++++++++++++++++++++++++++++++++++++++++

Add ewoks configuration to beacon.

Dependencies installed in the bliss environment

.. code-block:: bash

    pip install blissoda[streamline]

Install and configure the `streamline sample changer <https://gitlab.esrf.fr/bliss/streamline_changer/>`_.

Add this to the bliss session setup script that needs the streamline scanner

.. code-block:: python

    from blissoda.streamline.streamline_scanner import StreamlineScanner as _StreamlineScanner
    streamline_scanner = _StreamlineScanner()
