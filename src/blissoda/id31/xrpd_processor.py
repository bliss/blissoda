"""Automatic pyfai integration for every scan with saving and plotting"""

import os
from typing import Optional, List
from ..xrpd.processor import XrpdProcessor
from ..persistent.parameters import autocomplete_property, ParameterInfo

from ewoksutils.task_utils import task_inputs


try:
    from bliss import setup_globals
except ImportError:
    setup_globals = None


class Id31XrpdProcessor(
    XrpdProcessor,
    parameters=[
        ParameterInfo("pyfai_config", category="PyFai"),
        ParameterInfo("integration_options", category="PyFai"),
        ParameterInfo("flat_enabled", category="Flat-field"),
        ParameterInfo("newflat", category="Flat-field"),
        ParameterInfo("oldflat", category="Flat-field"),
        ParameterInfo(
            "ascii_options",
            category="Data saving",
        ),
    ],
):

    # Override XrpdProcessor default workflows
    DEFAULT_WORKFLOW = None
    DEFAULT_WORKFLOW_NO_SAVE = None

    def __init__(self, **defaults) -> None:
        if setup_globals is None:
            raise ImportError("requires a bliss session")
        defaults.setdefault(
            "integration_options",
            {
                "method": "no_csr_ocl_gpu",
                "nbpt_rad": 4096,
                "unit": "q_nm^-1",
            },
        )
        defaults.setdefault(
            "ascii_options",
            {
                "enabled": False,
                "scans": [
                    "ascan",
                    "dscan",
                    "a2scan",
                    "d2scan",
                    "loopscan",
                    "timescan",
                    "sct",
                ],
                "max_npoints": 1000,
            },
        )
        defaults.setdefault("flat_enabled", False)
        defaults.setdefault("newflat", "/data/id31/inhouse/P3/flats.mat")
        defaults.setdefault("oldflat", "/data/id31/inhouse/P3/flats_old.mat")

        super().__init__(**defaults)

        self._update_workflows()

    def get_config_filename(self, lima_name: str) -> Optional[str]:
        return self.pyfai_config

    def get_integration_options(self, lima_name: str) -> Optional[dict]:
        integration_options = self.integration_options
        if integration_options:
            return integration_options.to_dict()
        return None

    def get_inputs(self, scan, lima_name: str) -> List[dict]:
        inputs = super().get_inputs(scan, lima_name)
        inputs += task_inputs(
            task_identifier="FlatFieldFromEnergy",
            inputs={
                "newflat": self.newflat,
                "oldflat": self.oldflat,
                "energy": setup_globals.energy.position,
            },
        )

        ascii_basename_template = self._export_filename_prefix(scan) + "_%04d.xye"
        if scan.scan_info.get("npoints", float("inf")) == 1:
            output_filename_template = os.path.join(
                self._export_folder(scan), ascii_basename_template
            )
            output_archive_filename = ""
        else:  # Store ASCII files in a zip file if there is more than one point
            output_filename_template = ascii_basename_template
            output_archive_filename = os.path.join(
                self._export_folder(scan),
                self._export_filename_prefix(scan) + ".zip",
            )
        inputs += task_inputs(
            task_identifier="SaveNexusPatternsAsAscii",
            inputs={
                "enabled": self._is_save_ascii_enabled(scan),
                "output_filename_template": output_filename_template,
                "output_archive_filename": output_archive_filename,
            },
        )

        return inputs

    def _is_save_ascii_enabled(self, scan) -> bool:
        """Returns whether results need to be exported as ASCII for this scan"""
        if not self.ascii_options:
            return False

        ascii_options = self.ascii_options.to_dict()
        if not ascii_options.get("enabled", False):
            return False

        scan_type = scan.scan_info.get("type", "")
        # sct's scan_type is ct with saving enabled
        if scan_type == "ct" and scan.scan_info.get("save"):
            scan_type = "sct"

        return scan_type in ascii_options.get("scans", []) and (
            0
            < scan.scan_info.get("npoints", float("inf"))
            <= ascii_options.get("max_npoints", 0)
        )

    def _export_filename_prefix(self, scan) -> str:
        """Returns the prefix to use for exported filenames"""
        basename = os.path.basename(self.get_filename(scan))
        stem = os.path.splitext(basename)[0]
        scan_nb = scan.scan_info.get("scan_nb")
        return f"{stem}_{scan_nb:04d}"

    def _export_folder(self, scan) -> str:
        """Returns the folder where to store exported files"""
        return os.path.join(
            self.scan_processed_directory(scan),
            "export",
        )

    def get_workflow(self, scan) -> Optional[str]:
        return self._get_workflow(scan)

    def _get_default_workflow(self, scan) -> Optional[str]:
        # Make sure default workflows are never used
        return self._get_workflow(scan)

    def _update_workflows(self):
        if self.flat_enabled:
            self.workflow_with_saving = "integrate_with_saving_with_flat.json"
            self.workflow_without_saving = "integrate_without_saving_with_flat.json"
        else:
            self.workflow_with_saving = "integrate_with_saving.json"
            self.workflow_without_saving = "integrate_without_saving.json"

    def get_submit_arguments(self, scan, lima_name) -> dict:
        kwargs = super().get_submit_arguments(scan, lima_name)
        kwargs["outputs"] = [
            {"all": False},
            {"name": "nxdata_url", "task_identifier": "IntegrateBlissScan"},
        ]
        kwargs["load_options"] = {"root_module": "ewoksid31.workflows"}
        return kwargs

        # TODO: Redis events don't show up
        handler = {
            "class": "ewoksjob.events.handlers.RedisEwoksEventHandler",
            "arguments": [
                {
                    "name": "url",
                    "value": "redis://bibhelm:25001/4",
                },
                {"name": "ttl", "value": 86400},
            ],
        }
        kwargs["execinfo"] = {"handlers": [handler]}
        return kwargs

    @autocomplete_property
    def flat_enabled(self) -> bool:
        return self._get_parameter("flat_enabled")

    @flat_enabled.setter
    def flat_enabled(self, enabled: bool):
        self._set_parameter("flat_enabled", enabled)
        self._update_workflows()

    def enabled_flatfield(self, enable: bool) -> None:
        # Kept for backward compatibility
        self.flat_enabled = enable

    def ensure_workflow_accessible(self, scan) -> None:
        pass
