import os
import json
from typing import Optional, List

try:
    from bliss import setup_globals
except ImportError:
    setup_globals = None
try:
    from bliss import current_session
except ImportError:
    current_session = None

from .calib import DEFAULT_CALIB
from ..utils import directories
from ..xrpd.processor import XrpdProcessor
from ..persistent.parameters import ParameterInfo


class DemoXrpdProcessor(
    XrpdProcessor,
    parameters=[
        ParameterInfo("config_filename", category="PyFai"),
        ParameterInfo("integration_options", category="PyFai"),
    ],
):
    def __init__(self, **defaults) -> None:
        defaults.setdefault("lima_names", ["difflab6"])
        defaults.setdefault(
            "integration_options",
            {
                "method": "no_csr_cython",
                "nbpt_rad": 4096,
                "radial_range_min": 1,
                "unit": "q_nm^-1",
            },
        )
        super().__init__(**defaults)
        self._ensure_config_filename()

    def get_integrate_inputs(
        self, scan, lima_name: str, task_identifier: str
    ) -> List[dict]:
        self._ensure_config_filename()
        inputs = super().get_integrate_inputs(scan, lima_name, task_identifier)
        is_even = not bool(
            setup_globals.difflab6.image.width % 2
        )  # lima-camera-simulator<1.9.10 does not support odd image widths
        inputs.append(
            {"task_identifier": task_identifier, "name": "demo", "value": is_even}
        )
        return inputs

    def _ensure_config_filename(self):
        if self.config_filename:
            return
        root_dir = self._get_config_dir(current_session.scan_saving.filename)
        cfgfile = os.path.join(root_dir, "pyfaicalib.json")
        os.makedirs(os.path.dirname(cfgfile), exist_ok=True)
        poni = DEFAULT_CALIB
        with open(cfgfile, "w") as f:
            json.dump(poni, f)
        self.config_filename = cfgfile

    def _get_demo_result_dir(self, dataset_filename: str) -> str:
        root_dir = directories.get_processed_dir(dataset_filename)
        return os.path.join(root_dir, "demo", "xrpd")

    def _get_workflows_dir(self, dataset_filename: str) -> str:
        root_dir = self._get_demo_result_dir(dataset_filename)
        return os.path.join(root_dir, "workflows")

    def _get_config_dir(self, dataset_filename: str) -> str:
        root_dir = self._get_demo_result_dir(dataset_filename)
        return os.path.join(root_dir, "config")

    def get_config_filename(self, lima_name: str) -> Optional[str]:
        return self.config_filename

    def get_integration_options(self, lima_name: str) -> Optional[dict]:
        return self.integration_options.to_dict()


if setup_globals is None:
    xrpd_processor = None
else:
    try:
        xrpd_processor = DemoXrpdProcessor()
    except ImportError:
        xrpd_processor = None
