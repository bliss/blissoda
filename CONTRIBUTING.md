<a href="https://gitlab.esrf.fr/dau/ci/pyci/-/blob/main/CONTRIBUTING.md" target="_blank">CONTRIBUTING.md</a>

## Bliss and Blissdata support

| Bliss Version | BlissData Version |
|---------------|-------------------|
| master        | 2.0.x             |
| 2.1           | 1.1.x             |
| 2.0           | 1.0.x             |
| 1.11          | 0.3.x             |
